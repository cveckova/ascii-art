#include <stdio.h>
#include <math.h>

void print_circle(char n)
{
	int i,j;
	double pomocna;
	
	for(i=n;i>0;i--)
	{
		for(j=n;j>=-n;j--)
		{
			pomocna=i*i+j*j;
			pomocna=sqrt(pomocna);
			
			if((int)pomocna==n)
				printf("__");
			else 
				printf("..");
		}
		printf("\n");
	}
	
	for(i=0;i<=n;i++)
	{
		for(j=-n;j<=n;j++)
		{
			pomocna=i*i+j*j;
			pomocna=sqrt(pomocna);
			
			if((int)pomocna==n)
				printf("__");
			else 
				printf("..");
		}
		printf("\n");
	}
	
}

int main()
{
	int polomer;
	
	printf("Nacitaj polomer kruznice:");
	scanf("%d",&polomer);
	
	
	print_circle((char)polomer);
	return 0;
}
